var userInterface = (function() {
	var t = function(width, height, x, y, title) {
		
		var window_container = document.createElement("div");
		window_container.classList.add("window");
		window_container.style.width = width + "px";
		window_container.style.height = height + "px";
		window_container.style.position = "absolute";
		window_container.style.top = y + "px";
		window_container.style.left = x + "px";
		
		var close_button = document.createElement("input");
		close_button.type = "button";
		close_button.value = "x";
		close_button.style.width = "30px";
		close_button.style.height = "30px";
		close_button.style.display = "inline-block";
		close_button.onclick = function() {close_ui();};
		
		var drag_bar = document.createElement("input");
		drag_bar.type = "button";
		drag_bar.style.width = (width - 30) + "px";
		drag_bar.style.height = "30px";
		drag_bar.style.display = "inline-block";
		drag_bar.style.position = "absolute";
		drag_bar.value = title;
		drag_bar.onmousedown = function(e) {drag_ui(e);};
		
		
		var clickX = 0;
		var clickY = 0;
		
		window_container.appendChild(close_button);
		window_container.appendChild(drag_bar);
		
		document.body.appendChild(window_container);
		
		// ui def
		var close_ui = function() {
			document.body.removeChild(window_container);
		}
		
		this.close_ui = function() {
			document.body.removeChild(window_container);
		}
		
		var mouse_track = function(e) {
			var diffx = e.screenX - clickX;
			var diffy = e.screenY - clickY;
			
			clickX += diffx;
			clickY += diffy;
			
			x += diffx;
			y += diffy;
			
			window_container.style.top = y + "px";
			window_container.style.left = x + "px";
		}
		
		var drag_ui = function(e) {
			clickX = e.screenX;
			clickY = e.screenY;
			document.addEventListener('mousemove', mouse_track, false);
			document.onmouseup = function() {document.removeEventListener('mousemove', mouse_track);};
		}
		
		this.getWindowContainer = function() {
			return window_container;
		}
		
		this.addControl = function(control, data, width, height, x, y, parent, callback) {
			switch(control) {
				case "button":
					var button_control = document.createElement("input");
					button_control.type = "button";
					button_control.value = data;
					button_control.style.width = width + "px";
					button_control.style.height = height + "px";
					button_control.style.left = x + "px";
					button_control.style.top = (y + 30) + "px";
					button_control.style.display = "block";
					button_control.style.position = "absolute";
					button_control.style.padding = "0px";
					button_control.style.margin = "0px";
					button_control.onclick = function() {callback();};
					
					parent.getWindowContainer().appendChild(button_control);
					
					return button_control;
				break;
				
				case "textbox":
					var text_control = document.createElement("input");
					text_control.type = "text";
					text_control.value = data;
					text_control.style.width = width + "px";
					text_control.style.height = height + "px";
					text_control.style.left = x + "px";
					text_control.style.top = (y + 30) + "px";
					text_control.style.display = "block";
					text_control.style.position = "absolute";
					text_control.style.padding = "0px";
					text_control.style.margin = "0px";
					text_control.onclick = function() {callback();};
					
					parent.getWindowContainer().appendChild(text_control);
					
					return text_control;
				break;
				
				case "label":
					var label_control = document.createElement("p");
					label_control.innerHTML = data;
					label_control.style.width = width + "px";
					label_control.style.height = height + "px";
					label_control.style.left = x + "px";
					label_control.style.top = (y + 30) + "px";
					label_control.style.display = "block";
					label_control.style.position = "absolute";
					label_control.style.padding = "0px";
					label_control.style.margin = "0px";
					label_control.onclick = function() {callback();};
					
					parent.getWindowContainer().appendChild(label_control);
					
					return label_control;
				break;
				
				case "combo":
					var combo_control = document.createElement("select");
					
					for(var i = 0;i < data.length;i++) {
						combo_control.innerHTML += "<option>" + data[i] + "</option>";
					}
					
					combo_control.style.width = width + "px";
					combo_control.style.height = height + "px";
					combo_control.style.left = x + "px";
					combo_control.style.top = (y + 30) + "px";
					combo_control.style.display = "block";
					combo_control.style.position = "absolute";
					combo_control.style.padding = "0px";
					combo_control.style.margin = "0px";
					combo_control.onclick = function() {callback();};
					
					parent.getWindowContainer().appendChild(combo_control);
					
					return combo_control;
				break;
				
				default:
					throw "Unknown control passed " + control;
				break;
			}
		}
	}
	
	return t;
})();